commit 5a377945482d10f6e7eaccc3afbb11a246ac3d94
Author: Samuel Thibault <sthibault@debian.org>
Date:   Sun Sep 1 17:41:32 2024 +0200

    Add missing const qualifiers

diff --git a/src/netfs.c b/src/netfs.c
index 9a11dfcf4..a649d6b2a 100644
--- a/src/netfs.c
+++ b/src/netfs.c
@@ -196,7 +196,7 @@ error_t netfs_attempt_readlink (struct iouser *user, struct node *node,
    locked on success; no matter what, unlock DIR before returning.  */
 error_t
 netfs_attempt_create_file (struct iouser *user, struct node *dir,
-			   char *name, mode_t mode, struct node **node)
+			   const char *name, mode_t mode, struct node **node)
 {
   FUNC_PROLOGUE("netfs_attempt_create_file");
   error_t err;
@@ -312,7 +312,7 @@ netfs_attempt_statfs (struct iouser *cred, struct node *node,
 
       if(! err)
 	{
-	  st->f_type = stvfs.__f_type;
+	  st->f_type = stvfs.f_type;
 	  st->f_bsize = stvfs.f_bsize;
 	  st->f_blocks = stvfs.f_blocks;
 	  st->f_bfree = stvfs.f_bfree;
@@ -338,7 +338,7 @@ netfs_attempt_statfs (struct iouser *cred, struct node *node,
 /* Attempt to create a new directory named NAME in DIR for USER with mode
    MODE.  */
 error_t netfs_attempt_mkdir (struct iouser *user, struct node *dir,
-			     char *name, mode_t mode)
+			     const char *name, mode_t mode)
 {
   FUNC_PROLOGUE("netfs_attempt_mkdir");
   error_t err;
@@ -586,7 +586,7 @@ netfs_attempt_sync (struct iouser *cred, struct node *node, int wait)
  * The node DIR is locked, and shall stay locked
  */
 error_t netfs_attempt_unlink (struct iouser *user, struct node *dir,
-			      char *name)
+			      const char *name)
 {
   FUNC_PROLOGUE("netfs_attempt_unlink");
   error_t err;
@@ -738,7 +738,7 @@ netfs_report_access (struct iouser *cred, struct node *node, int *types)
    (*NODE, if found, should be locked, this call should unlock DIR no matter
    what.) */
 error_t netfs_attempt_lookup (struct iouser *user, struct node *dir,
-			      char *name, struct node **node)
+			      const char *name, struct node **node)
 {
   FUNC_PROLOGUE_FMT("netfs_attempt_lookup", "name=%s, dir=%s",
 		    name, dir->nn->path);
@@ -813,7 +813,7 @@ out:
    DIR nor FILE are locked.  If EXCL is set, do not delete the target, but
    return EEXIST if NAME is already found in DIR.  */
 error_t netfs_attempt_link (struct iouser *user, struct node *dir,
-			    struct node *file, char *name, int excl)
+			    struct node *file, const char *name, int excl)
 {
   FUNC_PROLOGUE_FMT("netfs_attempt_link", "link=%s/%s, to=%s",
 		    dir->nn->path, name, file->nn->path);
@@ -865,7 +865,7 @@ error_t netfs_attempt_link (struct iouser *user, struct node *dir,
 /* Attempt to remove directory named NAME in DIR for USER.
  * directory DIR is locked, and shall stay locked. */
 error_t netfs_attempt_rmdir (struct iouser *user,
-			     struct node *dir, char *name)
+			     struct node *dir, const char *name)
 {
   FUNC_PROLOGUE("netfs_attempt_rmdir");
   error_t err;
@@ -919,7 +919,7 @@ error_t netfs_attempt_chauthor (struct iouser *cred, struct node *node,
 
 /* Attempt to turn NODE (user CRED) into a symlink with target NAME. */
 error_t netfs_attempt_mksymlink (struct iouser *cred, struct node *node,
-				 char *name)
+				 const char *name)
 {
   FUNC_PROLOGUE_NODE("netfs_attempt_mksymlink", node);
   error_t err;
@@ -969,8 +969,8 @@ error_t netfs_attempt_mksymlink (struct iouser *cred, struct node *node,
 
 /* Note that in this one call, neither of the specific nodes are locked. */
 error_t netfs_attempt_rename (struct iouser *user, struct node *fromdir,
-			      char *fromname, struct node *todir,
-			      char *toname, int excl)
+			      const char *fromname, struct node *todir,
+			      const char *toname, int excl)
 {
   FUNC_PROLOGUE("netfs_attempt_rename");
   error_t err;
@@ -1051,7 +1051,7 @@ error_t netfs_attempt_rename (struct iouser *user, struct node *fromdir,
    to *LEN bytes from DATA.  Set *LEN to the amount seccessfully written upon
    return. */
 error_t netfs_attempt_write (struct iouser *cred, struct node *node,
-			     loff_t offset, size_t *len, void *data)
+			     loff_t offset, size_t *len, const void *data)
 {
   FUNC_PROLOGUE_NODE("netfs_attempt_write", node);
   error_t err;
